package tool

import (
	"database/sql"
	"fmt"
	"gitee.com/kristas/sfa-admin-iris/code/beans"
	"reflect"
	"strconv"
)

type Mapper struct {
	sql   string
	Table string
	Struct interface{}
}

func (m *Mapper) buildSql(example *Example) {
	//fieldNames := StructFieldNames(m.Struct)
	//join := strings.ToLower(strings.Join(fieldNames, ","))
	//fmt.Println("join", join)

	m.sql = "select " + "*" + " from " + m.Table
	m.sql += example.toSql() + example.sql
}

func (m *Mapper) Query(example Example) []interface{} {
	m.buildSql(&example)

	var db *sql.DB
	beans.Autowired(&db)

	fmt.Println("sql", m.sql)
	fmt.Println("param", example.params)
	stmt, _ := db.Prepare(m.sql)
	rows, _ := stmt.Query(example.params...)

	defer rows.Close()
	val := reflect.ValueOf(m.Struct).Elem()

	list := make([]interface{}, 0)
	col := make([]interface{}, val.NumField())
	for i := 0; i < val.NumField(); i++ {
		col[i] = val.Field(i).Addr().Interface()
	}
	for rows.Next() {
		rows.Scan(col...)
		list = append(list, val.Interface())
	}
	return list
}

func (m *Mapper) QueryRow(example Example) interface{} {
	m.buildSql(&example)

	var db *sql.DB
	beans.Autowired(&db)

	fmt.Println("sql", m.sql)
	fmt.Println("param", example.params)
	stmt, _ := db.Prepare(m.sql)
	row:= stmt.QueryRow(example.params...)

	val := reflect.ValueOf(m.Struct).Elem()

	col := make([]interface{}, val.NumField())
	for i := 0; i < val.NumField(); i++ {
		col[i] = val.Field(i).Addr().Interface()
	}
	row.Scan(col...)

	return val.Interface()
}

type Example struct {
	criterion []*criteria
	sql       string
	params    []interface{}
}

func (e *Example) CreateCriteria() *criteria {
	c := new(criteria)
	c.isOr = false
	e.criterion = append(e.criterion, c)
	return c
}

func (e *Example) Or(criteria *criteria) {
	criteria.isOr = true
}

func (e *Example) UsePage(pageNo, pageSize int) {
	limit := pageSize
	offset := (pageNo - 1) * limit
	e.sql += " limit " + strconv.Itoa(offset) + ", " + strconv.Itoa(limit)
}

func (e *Example) toSql() string {
	var sql string
	if len(e.criterion)!=0 {
		sql = " where"
	}
	for i, c := range e.criterion {
		if i != 0 {
			if c.isOr {
				sql += " or"
			} else {
				sql += " and"
			}
		}
		sql += c.toSql()
		e.params = append(e.params, c.params...)
	}
	return sql
}

type criteria struct {
	sql    string
	isOr   bool
	params []interface{}
}

func (c *criteria) AndEqual(col string, param ...interface{}) *criteria {
	generateParam(c, col, "=", param...)
	return c
}

func (c *criteria) AndLike(col string, param ...interface{}) *criteria {
	generateParam(c, col, "like", param...)
	return c
}

func (c *criteria) AndBigger(col string, param ...interface{}) *criteria {
	generateParam(c, col, ">", param...)
	return c
}

func (c *criteria) AndNotBigger(col string, param ...interface{}) *criteria {
	generateParam(c, col, "<=", param...)
	return c
}

func (c *criteria) AndLess(col string, param ...interface{}) *criteria {
	generateParam(c, col, "<", param...)
	return c
}

func (c *criteria) AndNotLess(col string, param ...interface{}) *criteria {
	generateParam(c, col, ">=", param...)
	return c
}

func (c *criteria) AndIn(col string, param ...interface{}) *criteria {
	generateParam(c, col, "in", param...)
	return c
}

func generateParam(c *criteria, col, split string, param ...interface{}) {
	if c.sql != "" {
		c.sql += " and "
	}
	c.sql += col + " " + split
	if len(param) == 1 {
		c.sql += " ?"
	} else {
		c.sql += " ("
		leng := len(param)
		for i := 1; i <= leng; i++ {
			c.sql += " ?"
			if i != leng {
				c.sql += ","
			}
		}
		c.sql += " )"
	}
	c.params = append(c.params, param...)
}

func (c *criteria) toSql() string {
	return " ( " + c.sql + " )"
}
