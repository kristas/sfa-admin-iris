package tool

import (
	"reflect"
)

func StructToMap(i interface{}) map[string]interface{} {
	val := reflect.ValueOf(i)
	typ := reflect.TypeOf(i)
	m := make(map[string]interface{})
	for i := 0; i < typ.NumField(); i++ {
		m[typ.Field(i).Name] = val.Field(i).Interface()
	}
	return m
}

func StructFieldNames(i interface{}) []string {
	arr:=make([]string,0)
	typ := reflect.TypeOf(i)
	for i := 0; i < typ.NumField(); i++ {
		arr=append(arr,typ.Field(i).Name)
	}
	return arr
}