package configs

import (
	"gitee.com/kristas/sfa-admin-iris/code/beans"
	"github.com/kataras/iris"
)

type Server struct {
	AppName string
	Port string
}

func ServerConfigurator(app *iris.Application) (){
	serv:=Server{}
	Autowired(&serv)
	beans.Set(serv)
}