package configs

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"reflect"
	"strings"
)

/**
通过配置文件反射出实体类
*/
func Autowired(bean interface{}) {
	mutable := reflect.ValueOf(bean).Elem()
	t := fmt.Sprintf("%s", mutable.Type())
	beanName:=t[strings.LastIndex(t,".")+1:]
	env:="dev"
	profileUrl:=fmt.Sprintf("./resources/app-%s.yml",env)
	file, err := ioutil.ReadFile(profileUrl)
	if err!=nil {
		panic("profile not exists.")
	}
	result := make(map[string]interface{})
	yaml.Unmarshal(file, &result)
	data := result[beanName].(map[interface{}]interface{})

	for k, v := range data {
		mutable.FieldByName(k.(string)).SetString(v.(string))
	}
}
