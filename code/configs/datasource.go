package configs

import (
	"database/sql"
	"fmt"
	"gitee.com/kristas/sfa-admin-iris/code/beans"
	_ "github.com/go-sql-driver/mysql"
	"github.com/kataras/golog"
	"github.com/kataras/iris"
	"github.com/kataras/iris/core/host"
)

type DataSource struct {
	Class    string
	Username string
	Password string
	Host     string
	Url      string
}

func DataSourceConfigurator(app *iris.Application) () {
	source := DataSource{}
	Autowired(&source)

	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s)/%s",
		source.Username, source.Password, source.Host, source.Url)
	db, err := sql.Open(source.Class, dataSourceName)
	if err != nil {
		golog.Error("failed to open database:", err.Error())
		return
	}
	beans.Set(db)
	golog.Info("Initializing dataSource")
	app.ConfigureHost(func(h *host.Supervisor) { // <- 这里: 很重要
		h.RegisterOnShutdown(func() {
			db.Close()
			golog.Info("Closed dataSource")
		})
	})
}
