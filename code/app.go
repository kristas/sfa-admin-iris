package main

import (
	"gitee.com/kristas/sfa-admin-iris/code/beans"
	"gitee.com/kristas/sfa-admin-iris/code/configs"
	_ "gitee.com/kristas/sfa-admin-iris/code/configs"
	"gitee.com/kristas/sfa-admin-iris/code/cons"
	"gitee.com/kristas/sfa-admin-iris/code/controller"
	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/logger"
	recover2 "github.com/kataras/iris/middleware/recover"
	"github.com/kataras/iris/mvc"
)

func main() {
	app := iris.New()
	app.Use(recover2.New())
	app.Use(logger.New())
	htmlEngine := iris.HTML("./resources/template", ".html")
	htmlEngine.Reload(true)
	app.RegisterView(htmlEngine)
	app.Configure(configs.DataSourceConfigurator, configs.ServerConfigurator)

	mvc.Configure(app.Party("/api/v1/users"), users)
	var serv configs.Server
	beans.Autowired(&serv)
	app.Run(iris.Addr(serv.Port), iris.WithConfiguration(iris.YAML(cons.CONFIG_DEV)))
}

func users(app *mvc.Application) {
	app.Handle(new(controller.UserController))
}
