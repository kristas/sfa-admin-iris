package beans

import (
	"github.com/kataras/golog"
	"reflect"
)

var beans = make(map[reflect.Type]reflect.Value)

func Set(bean interface{}) {
	typ := reflect.TypeOf(bean)
	value := reflect.ValueOf(bean)
	beans[typ] = value
	golog.Infof("BeanFactory receive: %s", typ)
}

func Autowired(bean interface{}) {
	mutable := reflect.ValueOf(bean).Elem()
	t := mutable.Type()
	if value, ok := beans[t]; ok {
		mutable.Set(value)
	} else {
		golog.Errorf("No beans for %s",t)
	}
}
