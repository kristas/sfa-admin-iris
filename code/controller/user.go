package controller

import (
	"gitee.com/kristas/sfa-admin-iris/code/entity"
	"gitee.com/kristas/sfa-admin-iris/code/tool"
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
)

type UserController struct {
}

func (u *UserController) BeforeActivation(b mvc.BeforeActivation) {
	b.Handle("GET", "/enabled/{enabled:bool}", "GetUserByEnabled")
}

func (u *UserController) Get(ctx iris.Context) []interface{} {
	pageSize, _ := ctx.URLParamInt("pageSize")
	pageNo, _ := ctx.URLParamInt("pageNo")
	mapper := tool.Mapper{
		Table:   "oauth_user",
		Struct: &entity.Users{},
	}
	example := tool.Example{}
	example.UsePage(pageNo,pageSize)
	list := mapper.Query(example)
	return list
}

func (u *UserController) GetBy(username string, ctx iris.Context) {
	mapper := tool.Mapper{
		Table:  "oauth_user",
		Struct: &entity.Users{},
	}
	example := tool.Example{}
	example.CreateCriteria().AndEqual("user_id",username)
	user := mapper.QueryRow(example)
	ctx.ViewData("message", user)
	ctx.View("index.html")
}

func (u *UserController) GetUserByEnabled(enabled bool, ctx iris.Context) {
	mapper := tool.Mapper{
		Table:  "oauth_user",
		Struct: &entity.Users{},
	}
	example := tool.Example{}
	example.CreateCriteria().AndEqual("enabled",enabled)
	user := mapper.QueryRow(example)
	ctx.ViewData("message", user)
	ctx.View("index.html")
}
